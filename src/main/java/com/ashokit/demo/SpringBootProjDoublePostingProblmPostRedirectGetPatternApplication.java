package com.ashokit.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootProjDoublePostingProblmPostRedirectGetPatternApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootProjDoublePostingProblmPostRedirectGetPatternApplication.class, args);
	}

}
