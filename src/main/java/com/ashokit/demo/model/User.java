package com.ashokit.demo.model;

import lombok.Data;

@Data
public class User {
	private String userId;
	private String uname;
	private String email;
	private Long phno;

}
