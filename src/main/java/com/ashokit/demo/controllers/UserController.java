package com.ashokit.demo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ashokit.demo.model.User;

@Controller
@RequestMapping("/user")
public class UserController {
system.out.println("HIS-10Changes");




System.out.println("usercontroller");

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@RequestMapping(value = { "/", "createUserAcc" }, method = RequestMethod.GET)
	public String loadUserForm(Model model) {

		model.addAttribute("userModel", new User());
System.out.println("UserController.loadUserForm()");
		// return logical view

		return "createUserAcc";

	}

	@RequestMapping(value = "/createUser", method = RequestMethod.POST)
	public String createUserAcc(@ModelAttribute("userModel") User user,RedirectAttributes attributes) {

		logger.info("User Form Submited::" + user);

		/// logic to insert data into db

		//model.addAttribute("msg", "Account Created Successfully");
		
		attributes.addFlashAttribute("msg", "Account Created Successfully");
		System.out.println("UserController.createUserAcc()");
		return "redirect:userAccCreationSuccess";
		
	}

	
	  @RequestMapping(value = "/userAccCreationSuccess",method = RequestMethod.GET)
	  public String userAccCreationSuccess(Model model) {
	  
	  logger.info("userAccCreationSuccess()method call");
	  
	  model.addAttribute("userModel",new User()); 
	  return "createUserAcc";
	  
	  }
	 
	  
	  
	 

}
