<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Registration Form</title>
</head>
<body>
<font color='Green'>${msg}</font>

<h3>User Registration Form</h3>
<form:form action="createUser" modelAttribute="userModel" method="POST">
<table>
<tr>
<td>Username</td>
<td><form:input path="uname"/></td>
</tr>

<tr>
<td>Email</td>
<td><form:input path="email"/></td>
</tr>


<tr>
<td>Phone Number</td>
<td><form:input path="phno"/></td>
</tr>

<tr>
<td><input type="reset" value="Reset"></td>
<td><input type="submit" value="Submit"></td>
</tr>


</table>


</form:form>
</body>
</html>